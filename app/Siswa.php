<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Siswa extends Model
{
    protected $table = "siswas";
    protected $fillable = ['nama', 'nik', 'alamat', 'user_id'];

    use SoftDeletes;
}
