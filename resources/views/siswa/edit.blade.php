@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    <form action="{{ url('siswa',$siswa->id) }}" method="post">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label>Nama</label>
                            <input type="text" name="nama" value="{{ $siswa->nama }}" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>NIK</label>
                            <input type="text" name="nik" value="{{ $siswa->nik }}" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Alamat</label>
                            <input type="text" name="alamat" value="{{ $siswa->alamat }}" class="form-control">
                        </div>
                        <button type="submit" class="btn btn-primary btn-sm">Submit</button>
                        <a href="{{ url('siswa') }}" class="btn btn-danger btn-sm">cancel</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
