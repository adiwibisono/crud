<?php



Route::get('/', function () {
    return view('siswa.index');
});

Route::group(['middleware'=>'auth:web'], function() {
    Route::get('siswa', 'SiswaController@index')->middleware('verified');
    Route::get('siswa/create', 'SiswaController@create');
    Route::post('siswa', 'SiswaController@store');
    Route::get('siswa/{id}/edit', 'SiswaController@edit');
    Route::put('siswa/{id}', 'SiswaController@update');
    Route::delete('siswa/{id}', 'SiswaController@destroy');
    Route::get('/siswa/export', 'SiswaController@export_excel');
    Route::post('/siswa/import_excel', 'SiswaController@import_excel');
    Route::get('auth/telegram/callback', 'AuthController@handleTelegramCallback')->name('auth.telegram.handle');

});

Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home');

